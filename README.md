# retwis-py

Follows official tutorial from [Python Twitter Clone on REDIS](https://redis.io/topics/twitter-clone)

## REDIS

Adaptation from [Python Twitter clone](https://github.com/pims/retwis-py/) to run on flask

## Cassandra

Import implementation from [Twisandra](https://github.com/twissandra/twissandra)

# Running

Run app directly on flask

```
PYTHONPATH=. FLASK_APP=retwis FLASK_ENV=development DATABASE=redis flask run
```

Following configurations are available:

* `DATABASE`: accepting one between `redis` and `cassandra`

## Initialize Database

Supply `DATABASE` var and execute initialize script:

```
PYTHONPATH=. FLASK_APP=retwis FLASK_ENV=development DATABASE=cassandra flask init-db
```

