import json
import datetime

from bson import json_util
from bson.objectid import ObjectId

from .base import (Model, USER_TABLE, POST_TABLE, TIMELINE_TABLE, MENTIONS_TABLE,
                   FOLLOWERS_TABLE, FRIENDS_TABLE)
from retwis import mongodb


class User(Model):
    def __init__(self, *args, **kwargs):
        self.username = kwargs.pop('username')
        self.password = kwargs.pop('password')
        super(User, self).__init__(*args, **kwargs)

    @staticmethod
    def find_by_username(username, db):
        user_table = db[USER_TABLE]
        result = user_table.find_one({"username": username})
        if result is not None:
            result = json.loads(json_util.dumps(result))
            return User(
                id=result['_id']['$oid'],
                db=db,
                username=result['username'],
                password=result['password']
            )
        else:
            return None

    @staticmethod
    def find_by_id(user_id, db):
        user_table = db[USER_TABLE]
        result = user_table.find_one({"_id": ObjectId(user_id)})
        if result is not None:
            result = json.loads(json_util.dumps(result))
            return User(
                id=result['_id']['$oid'],
                db=db,
                username=result['username'],
                password=result['password']
            )
        else:
            return None

    @staticmethod
    def create(username, password, db):
        result = User.find_by_username(username=username, db=db)
        if not result:
            user = {
                'username': username,
                'password': password
            }
            _id = db[USER_TABLE].insert_one(user).inserted_id
            return User(
                id=_id,
                db=db,
                username=user['username'],
                password=user['password']
            )
        return None

    def posts(self, start=1, limit=10):
        post_table = self.db[POST_TABLE]
        posts = post_table.find({'username': self.username}).skip(start).limit(limit)
        if posts:
            posts = json.loads(json_util.dumps(posts))
            return [mongodb.Post(
                id=elm['_id']['$oid'], db=self.db, content=elm['content'], username=elm['username']) for elm in posts]
        return []

    def timeline(self, start=0, limit=10):
        timeline_table = self.db[f"{TIMELINE_TABLE}_{self.username}"]
        timeline = timeline_table.find().skip(start).limit(limit)
        if timeline:
            timeline = json.loads(json_util.dumps(timeline))
            return [mongodb.Post(
                id=elm['_id']['$oid'], db=self.db, content=elm['content'], username=elm['username']) for elm in timeline]
        return []

    def mentions(self, start=1, limit=10):
        mentions_table = self.db[f"{MENTIONS_TABLE}"]
        mentions = mentions_table.find({'username': self.username}).skip(start).limit(limit)
        if mentions:
            mentions = json.loads(json_util.dumps(mentions))
            return [mongodb.Post(
                id=elm['_id']['$oid'], db=self.db, content=elm['content'], username=elm['username']) for elm in mentions]
        return []

    def add_post(self, post, db):
        post_table = db[POST_TABLE]
        _id = post_table.insert_one({
            'content': post.content,
            'username': post.username
        }).inserted_id
        self.add_timeline_post(post)

    def add_timeline_post(self, post):
        post_table = self.db[f"{TIMELINE_TABLE}_{self.username}"]
        _id = post_table.insert_one({
            'post_id': post.id,
            'content': post.content,
            'username': post.username
        }).inserted_id

    def add_mention(self, post):
        mention_table = self.db[f"{MENTIONS_TABLE}"]
        _id = mention_table.insert_one({
            'post_id': post.id,
            'username': post.username,
            'content': post.content
        }).inserted_id

    def follow(self, user):
        """
        Start following a user involves register somebody you follow (friend)
        and register yourself on user followers list (follower)

        :param user:
        :return:
        """
        if user == self:
            return
        friends_table = self.db[FRIENDS_TABLE]
        now = datetime.datetime.utcnow()
        friends_table.insert_one({
            'user': self.id,
            'friend': user.id,
            'since': now
        })
        user.add_follower(self)

    def stop_following(self, user):
        friends_table = self.db[FRIENDS_TABLE]
        friends_table.delete_one({'friend': user.id})
        user.remove_follower(self)

    def following(self, user):
        friends_table = self.db[FRIENDS_TABLE]
        result = friends_table.find_one({'friend': user.id})
        if result is None:
            return False
        return True

    @property
    def followers(self):
        followers_table = self.db[FOLLOWERS_TABLE]
        result = followers_table.find({'user': self.id})
        if result:
            result = json.loads(json_util.dumps(result))
            return [User.find_by_id(user_id=user['follower'], db=self.db) for user in result]
        return []

    @property
    def followees(self):
        friends_table = self.db[FRIENDS_TABLE]
        result = friends_table.find({'user': self.id})
        if result:
            result = json.loads(json_util.dumps(result))
            return [User.find_by_id(user_id=user['friend'], db=self.db) for user in result]
        return []

    # added
    @property
    def tweet_count(self):
        post_table = self.db[POST_TABLE]
        return post_table.count_documents({'username': self.username})

    @property
    def followees_count(self):
        friends_table = self.db[FRIENDS_TABLE]
        return friends_table.count_documents({'user': self.id})

    @property
    def followers_count(self):
        followers_table = self.db[FOLLOWERS_TABLE]
        return followers_table.count_documents({'user': self.id})

    def add_follower(self, user):
        followers_table = self.db[FOLLOWERS_TABLE]
        now = datetime.datetime.utcnow()
        followers_table.insert_one({
            'user': user.id,
            'follower': self.id,
            'since': now
        })

    def remove_follower(self, user):
        followers_table = self.db[FOLLOWERS_TABLE]
        followers_table.delete_one({'follower': user.id})
