import json
import re

from bson import json_util

from retwis.mongodb.base import Model, POST_TABLE, TIMELINE_TABLE
from retwis import mongodb


class Post(Model):
    def __init__(self, *args, **kwargs):
        self.content = kwargs.pop('content')
        self.username = kwargs.pop('username')
        super(Post, self).__init__(*args, **kwargs)

    @staticmethod
    def create(user, content, db):
        post_table = db[POST_TABLE]
        data = {
            'content': content,
            'username': user.username
        }
        _id = post_table.insert_one(data).inserted_id
        post = Post(
            id=_id,
            db=db,
            username=data['username'],
            content=data['content']
        )
        # Add on user timeline post first
        post.user.add_timeline_post(post)

        for follower in user.followers:
            follower.add_timeline_post(post)

        mentions = re.findall(r'@\w+', content)
        for mention in mentions:
            u = mongodb.User.find_by_username(mention[1:], db)
            if u:
                u.add_mention(post, db)

        return post

    @staticmethod
    def find_by_id(post_id, db):
        post_table = db[POST_TABLE]
        result = post_table.find_one({"_id": post_id})
        if result is not None:
            result = json.loads(json_util.dumps(result))
            return Post(
                id=post_id,
                db=db,
                username=result['username'],
                content=result['content']
            )
        else:
            return None

    @property
    def user(self):
        return mongodb.user.User.find_by_username(username=self.username, db=self.db)
