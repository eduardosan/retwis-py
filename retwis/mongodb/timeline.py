import json

from bson import json_util

from retwis import mongodb
from retwis.mongodb.base import POST_TABLE


class Timeline:
    def __init__(self, db):
        self.db = db

    def page(self, start, limit=10):
        post_table = self.db[POST_TABLE]
        posts = post_table.find().skip(start).limit(limit)
        if posts:
            posts = json.loads(json_util.dumps(posts))
            return [mongodb.Post(
                id=elm['_id']['$oid'], db=self.db, content=elm['content'], username=elm['username']) for elm in posts]
        return []
