# Base model
USER_TABLE = 'user'
POST_TABLE = 'post'
TIMELINE_TABLE = 'timeline'
MENTIONS_TABLE = 'mentions'
FRIENDS_TABLE = 'friends'
FOLLOWERS_TABLE = 'followers'


class Model(object):
    def __init__(self, id, db):
        self.__dict__['id'] = id
        self.db = db

    def __eq__(self, other):
        return self.id == other.id
