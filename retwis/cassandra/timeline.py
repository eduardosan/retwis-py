from uuid import UUID


class Timeline:
    def __init__(self, db):
        self.db = db

    def _get_line(self, limit, table, username, start=None):
        """
        Gets a timeline or a userline given a username, a start, and a limit.
        """
        get_tweets_query = self.db.prepare("""
            SELECT * FROM tweets WHERE tweet_id=?
        """)

        # First we need to get the raw timeline (in the form of tweet ids)
        query = "SELECT time, tweet_id FROM {table} WHERE username=%s {time_clause} LIMIT %s"

        # See if we need to start our page at the beginning or further back
        if start is None:
            time_clause = ''
            params = (username, limit)
        else:
            time_clause = 'AND time < %s'
            params = (username, UUID(start), limit)

        query = query.format(table=table, time_clause=time_clause)

        results = list(self.db.execute(query, params))
        if len(results) == 0:
            return [], None

        # If we didn't get to the end, return a starting point for the next page
        if len(results) == limit:
            # Find the oldest ID
            oldest_timeuuid = min(row.time for row in results)

            # Present the string version of the oldest_timeuuid for the UI
            next_timeuuid = oldest_timeuuid.urn[len('urn:uuid:'):]
        else:
            next_timeuuid = None

        # Now we fetch the tweets themselves
        futures = []
        for row in results:
            futures.append(self.db.execute_async(
                get_tweets_query, (row.tweet_id,)))

        tweets = [f.result()[0] for f in futures]
        return tweets, next_timeuuid

    def page(self, username, start=None, limit=10, table='userline'):
        return self._get_line(username=username, start=start, limit=limit, table=table)
