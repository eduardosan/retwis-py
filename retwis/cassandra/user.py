# Fonte: https://github.com/twissandra/twissandra
import datetime

from uuid import uuid1, UUID, uuid4

from retwis.cassandra.base import Model, _timestamp_to_uuid, PUBLIC_USERLINE_KEY
from retwis import cassandra


class User(Model):
    def __init__(self, *args, **kwargs):
        super(User, self).__init__(*args, **kwargs)

    @staticmethod
    def find_by_username(username, db):
        """
        Serch a given username

        :param username:
        :param db:
        :return:
        """
        get_usernames_query = db.prepare("""SELECT * FROM users WHERE username=?""")
        rows = db.execute(get_usernames_query, (username,))
        if not rows:
            return None
        else:
            user_data = rows[0]
            user_obj = User(
                id=user_data.username,
                db=db
            )
            user_obj.username = user_data.username
            user_obj.password = user_data.password_hash
            return user_obj

    @staticmethod
    def find_by_id(user_id, db):
        """In a distributed environment, numeric IDs make no sense"""
        return User.find_by_username(username=user_id, db=db)

    @staticmethod
    def create(username, password, db):
        """
        Create new user and retorn created object

        :param username:
        :param password:
        :param db:
        :return:
        """
        add_user_query = db.prepare("""
            INSERT INTO users (username, password_hash)
            VALUES (?, ?)
        """)
        db.execute(add_user_query, (username, password))
        return User(id=username, db=db)

    def posts(self, start=None, limit=10):
        """
        Get posts from userline (their posts)

        :param start:
        :param limit:
        :return:
        """
        time = cassandra.Timeline(db=self.db)
        results, next = time.page(start=start, limit=limit, table="userline", username=self.username)
        posts = []
        for elm in results:
            posts.append(cassandra.Post(id=elm.tweet_id, db=self.db))
        return posts

    def timeline(self, start=None, limit=10):
        """
        Get posts from user timeline (users they follow)

        :param start:
        :param limit:
        :return:
        """
        time = cassandra.Timeline(db=self.db)
        results, next = time.page(start=start, limit=limit, table="timeline", username=self.username)
        posts = []
        for elm in results:
            posts.append(cassandra.Post(id=elm.tweet_id, db=self.db))
        return posts

    def mentions(self, page=1):
        """TODO: Include this on datamodel"""
        return []

    def add_post(self, post, timestamp=None, tweet_id=None):
        """
        Add post on followers timeline (others users)

        :param post:
        :param timestamp:
        :param tweet_id:
        :return:
        """
        timeline_query = self.db.prepare("""
            INSERT INTO timeline (username, time, tweet_id)
            VALUES (?, ?, ?)
        """)

        if timestamp is None:
            now = uuid1()
        else:
            now = _timestamp_to_uuid(timestamp)

        if tweet_id is None:
            tweet_id = self.add_timeline_post(post=post, timestamp=timestamp)

        # Insert tweet into the public timeline
        self.db.execute(timeline_query, (PUBLIC_USERLINE_KEY, now, tweet_id,))

        # Get the user's followers, and insert the tweet into all of their streams
        futures = []
        follower_usernames = [self.id] + self.followers
        for follower_username in follower_usernames:
            futures.append(self.db.execute_async(
                timeline_query, (follower_username, now, tweet_id,)))

        for future in futures:
            future.result()

        return tweet_id

    def add_timeline_post(self, post, timestamp=None):
        """
        Add post on userline (his timeline)

        :param post:
        :param timestamp:
        :return:
        """
        tweet = cassandra.Post.create(
            user=self.id,
            content=post,
            db=self.db,
            timestamp=timestamp
        )
        return tweet.id

    def add_mention(self, post):
        """TODO: Include mention on datamodel"""

    def follow(self, user):
        """
        Start following a user involves register somebody you follow (friend)
        and register yourself on user followers list (follower)

        :param user:
        :return:
        """
        friends_query = self.db.prepare("""
            INSERT INTO friends (username, friend, since)
            VALUES (?, ?, ?)
        """)
        followers_query = self.db.prepare("""
            INSERT INTO followers (username, follower, since)
            VALUES (?, ?, ?)
        """)

        now = datetime.datetime.utcnow()
        # 1. Start following user
        # 2. Add yourself as a follower of the user
        futures = [self.db.execute_async(friends_query, (self.id, user, now,)),
                   self.db.execute_async(followers_query, (user, self.id, now,))]

        for future in futures:
            future.result()

    def stop_following(self, user):
        """
        Remove yourself as a friend and remove user from your followers list

        :param user:
        :return:
        """
        remove_friends_query = self.db.prepare("""
            DELETE FROM friends WHERE username=? AND friend=?
        """)
        remove_followers_query = self.db.prepare("""
            DELETE FROM followers WHERE username=? AND follower=?
        """)

        futures = [self.db.execute_async(remove_friends_query, (self.id, user,)),
                   self.db.execute_async(remove_followers_query, (user, self.id,))]

        for future in futures:
            future.result()

    def following(self, user):
        """
        Check if user is being followed by you

        :param user:
        :return:
        """
        following_query = self.db.prepare("""
            SELECT *
            FROM friends
            WHERE username=? AND friend=?
        """)
        rows = self.db.execute(following_query, (self.id, user.username))
        if rows:
            return True
        return False

    @property
    def followers(self):
        """
        List of people that follow you
        :return:
        """
        count = 5000
        get_followers_query = self.db.prepare("""
            SELECT follower FROM followers WHERE username=? LIMIT ?
        """)

        rows = self.db.execute(get_followers_query, (self.id, count))
        return [row.follower for row in rows]

    @property
    def followees(self):
        """List of people you follow"""
        count = 5000
        get_followees_query = self.db.prepare("""
            SELECT friend FROM friends 
            WHERE username=? 
            LIMIT ?
        """)
        rows = self.db.execute(get_followees_query, (self.id, count))

        return [row.friend for row in rows]

    # added
    @property
    def tweet_count(self):
        """Total tweets you posted (your userline)"""
        get_tweets = self.db.prepare("""
            SELECT COUNT(*)
            FROM userline
            WHERE username=?
        """)
        rows = self.db.execute(get_tweets, (self.id,))
        return len(list(rows))

    @property
    def followees_count(self):
        """Total people following you"""
        get_followees_query = self.db.prepare("""
            SELECT COUNT(*)
            FROM friends
            WHERE username=?
        """)
        rows = self.db.execute(get_followees_query, (self.id,))
        return len(list(rows))

    @property
    def followers_count(self):
        """Total people you follow"""
        get_followers_query = self.db.prepare("""
            SELECT COUNT(*)
            FROM followers
            WHERE username=?
        """)
        rows = self.db.execute(get_followers_query, (self.id,))
        return len(list(rows))

    def add_follower(self, user):
        """Add a follower to your followers list"""
        followers_query = self.db.prepare("""
            INSERT INTO followers (username, follower, since)
            VALUES (?, ?, ?)
        """)

        now = datetime.datetime.utcnow()
        self.db.execute(followers_query, (self.id, user, now,))

    def remove_follower(self, user):
        """Remove follower from your followers list"""
        remove_followers_query = self.db.prepare("""
            DELETE FROM followers 
            WHERE username=?
            AND follower=?
        """)

        now = datetime.datetime.utcnow()
        self.db.execute(remove_followers_query, (self.id, user, now,))
