from uuid import uuid1, uuid4, UUID

from retwis.cassandra.base import Model, _timestamp_to_uuid
from retwis import cassandra


class Post(Model):
    def __init__(self, *args, **kwargs):
        super(Post, self).__init__(*args, **kwargs)

    @staticmethod
    def create(user, content, db, timestamp=None):
        tweets_query = db.prepare("""
            INSERT INTO tweets (tweet_id, username, body)
            VALUES (?, ?, ?)
        """)
        userline_query = db.prepare("""
            INSERT INTO userline (username, time, tweet_id)
            VALUES (?, ?, ?)
        """)
        tweet_id = uuid4()

        if timestamp is None:
            now = uuid1()
        else:
            now = _timestamp_to_uuid(timestamp)

        # Insert the tweet
        db.execute(tweets_query, (tweet_id, user.username, content,))
        # Insert tweet into the user's timeline
        db.execute(userline_query, (user.username, now, tweet_id,))

        return tweet_id

    @staticmethod
    def find_by_id(post_id, db):
        get_tweets_query = db.prepare("""
            SELECT * FROM tweets WHERE tweet_id=?
        """)

        results = db.execute(get_tweets_query, (UUID(post_id),))
        if not results:
            None

        post_obj = Post(id=post_id, db=db)
        post_obj.content = results[0].body
        return post_obj

    @property
    def user(self):
        get_tweets_user_query = self.db.prepare("""
            SELECT username FROM tweets WHERE tweet_id=?
        """)

        results = self.db.execute(get_tweets_user_query, (self.id,))
        if not results:
            None

        return cassandra.User.find_by_username(username=results[0].username, db=self.db)
