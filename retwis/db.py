import os.path
import redis
import click

from cassandra.cluster import Cluster
from pymongo import MongoClient

from flask import current_app, g
from flask.cli import with_appcontext


def get_db():
    db_type = current_app.config['DATABASE']

    if 'db' not in g:
        if db_type == 'redis':
            g.db = redis.Redis(
                host=current_app.config['DATABASE_HOST'],
                port=current_app.config['DATABASE_PORT'],
                db=current_app.config['DATABASE_NAME'])
        elif db_type == 'cassandra':
            cluster = Cluster([current_app.config['DATABASE_HOST']],
                              port=current_app.config['DATABASE_PORT'])
            g.db = cluster.connect(current_app.config['DATABASE_NAME'])
        elif db_type == 'mongodb':
            client = MongoClient(current_app.config['DATABASE_HOST'],
                                 current_app.config['DATABASE_PORT'])
            g.db = client[current_app.config['DATABASE_NAME']]

    return g.db


def close_db(e=None):
    db = g.pop('db', None)

    if db == 'redis':
        db.close()


def init_db():
    db = get_db()

    # Just execute startup script if supplied. Most NoSQL databases don't have it
    filename = current_app.config.get('DATABASE_SCRIPT')
    if filename:
        this_dir = os.path.dirname(os.path.realpath(__file__))
        sql_dir = os.path.join(this_dir, './sql/')
        with current_app.open_resource(os.path.join(sql_dir, filename)) as f:
            txt = f.read().decode('utf8')
            print(txt)
            stmts = txt.split(';')
            for i in stmts:
                stmt = i.strip()
                if stmt != '':
                    print('Executing "' + stmt + '"')
                    db.execute(stmt)


@click.command('init-db')
@with_appcontext
def init_db_command():
    """Clear the existing data and create new tables."""
    init_db()
    click.echo('Initialized the database.')


def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)
