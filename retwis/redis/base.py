# Base model

class Model(object):
    def __init__(self, id, db):
        self.__dict__['id'] = id
        self.db = db

    def __eq__(self, other):
        return self.id == other.id

    def __getattr__(self, name):
        if name not in self.__dict__:
            klass = self.__class__.__name__.lower()
            if name != 'db':
                v = self.db.get('%s:id:%s:%s' % (klass, self.id, name.lower()))
                if v:
                    return v.decode("utf-8")
                raise AttributeError('%s doesn\'t exist' % name)
        else:
            return self.__dict__[name]

    def __setattr__(self, name, value):
        if name not in self.__dict__:
            self.__dict__[name] = value
            if name != 'db':
                klass = self.__class__.__name__.lower()
                key = '%s:id:%s:%s' % (klass, self.id, name.lower())
                self.db.set(key, value)
        else:
            self.__dict__[name] = value
