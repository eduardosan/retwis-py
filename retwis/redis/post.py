import re

from retwis.redis.base import Model
from retwis import redis


class Post(Model):
    def __init__(self, *args, **kwargs):
        super(Post, self).__init__(*args, **kwargs)

    @staticmethod
    def create(user, content, db):
        post_id = db.incr("post:uid")
        post = Post(post_id, db)
        post.content = content
        post.user_id = user.id
        # post.created_at = Time.now.to_s
        user.add_post(post, db)
        db.lpush("timeline", post_id)
        for follower in user.followers:
            follower.add_timeline_post(post)

        mentions = re.findall(r'@\w+', content)
        for mention in mentions:
            u = redis.User.find_by_username(mention[1:], db)
            if u:
                u.add_mention(post, db)

    @staticmethod
    def find_by_id(post_id, db):
        if db.sismember('posts:id', int(post_id)):
            return Post(post_id, db)
        return None

    @property
    def user(self):
        user_id = self.db.get("post:id:%s:user_id" % self.id).decode('utf-8')
        result = redis.user.User.find_by_id(user_id, db=self.db)
        return result
