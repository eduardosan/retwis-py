from retwis.redis.base import Model
from retwis import redis


class User(Model):
    def __init__(self, *args, **kwargs):
        super(User, self).__init__(*args, **kwargs)

    @staticmethod
    def find_by_username(username, db):
        _id = db.get("user:username:%s" % username)
        if _id is not None:
            return User(int(_id), db=db)
        else:
            return None

    @staticmethod
    def find_by_id(user_id, db):
        if db.exists("user:id:%s:username" % user_id):
            return User(int(user_id), db=db)
        else:
            return None

    @staticmethod
    def create(username, password, db):
        user_id = db.incr("user:uid")
        if not db.get("user:username:%s" % username):
            db.set("user:id:%s:username" % user_id, username)
            db.set("user:username:%s" % username, user_id)

            db.set("user:id:%s:password" % user_id, password)
            db.lpush("users", user_id)
            return User(user_id, db=db)
        return None

    def posts(self, page=1,):
        _from, _to = (page - 1) * 10, page * 10
        posts = self.db.lrange("user:id:%s:posts" % self.id, _from, _to)
        if posts:
            return [redis.Post(int(post_id), db=self.db) for post_id in posts]
        return []

    def timeline(self, page=1):
        _from, _to = (page - 1) * 10, page * 10
        timeline = self.db.lrange("user:id:%s:timeline" % self.id, _from, _to)
        if timeline:
            return [redis.Post(int(post_id), db=self.db) for post_id in timeline]
        return []

    def mentions(self, page=1):
        _from, _to = (page - 1) * 10, page * 10
        mentions = self.db.lrange("user:id:%s:mentions" % self.id, _from, _to)
        if mentions:
            return [redis.Post(int(post_id), db=self.db) for post_id in mentions]
        return []

    def add_post(self, post, db):
        db.lpush("user:id:%s:posts" % self.id, post.id)
        db.lpush("user:id:%s:timeline" % self.id, post.id)
        db.sadd('posts:id', post.id)

    def add_timeline_post(self, post):
        self.db.lpush("user:id:%s:timeline" % self.id, post.id)

    def add_mention(self, post):
        self.db.lpush("user:id:%s:mentions" % self.id, post.id)

    def follow(self, user):
        if user == self:
            return
        else:
            self.db.sadd("user:id:%s:followees" % self.id, user.id)
            user.add_follower(self)

    def stop_following(self, user):
        self.db.srem("user:id:%s:followees" % self.id, user.id)
        user.remove_follower(self)

    def following(self, user):
        if self.db.sismember("user:id:%s:followees" % self.id, user.id):
            return True
        return False

    @property
    def followers(self):
        followers = self.db.smembers("user:id:%s:followers" % self.id)
        if followers:
            return [User(int(user_id)) for user_id in followers]
        return []

    @property
    def followees(self):
        followees = self.db.smembers("user:id:%s:followees" % self.id)
        if followees:
            return [User(int(user_id)) for user_id in followees]
        return []

    # added
    @property
    def tweet_count(self):
        return self.db.llen("user:id:%s:posts" % self.id) or 0

    @property
    def followees_count(self):
        return self.db.scard("user:id:%s:followees" % self.id) or 0

    @property
    def followers_count(self):
        return self.db.scard("user:id:%s:followers" % self.id) or 0

    def add_follower(self, user):
        self.db.sadd("user:id:%s:followers" % self.id, user.id)

    def remove_follower(self, user):
        self.db.srem("user:id:%s:followers" % self.id, user.id)
