from retwis.redis.post import Post


class Timeline:
    def __init__(self, db):
        self.db = db

    def page(self, page):
        _from = (page-1)*10
        _to = page*10
        return [Post(post_id, db=self.db) for post_id in self.db.lrange('timeline', _from, _to)]
